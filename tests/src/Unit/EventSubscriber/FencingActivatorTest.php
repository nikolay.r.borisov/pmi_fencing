<?php

namespace Drupal\Tests\pmi_fencing\Unit\EventSubscriber;

use Drupal\Tests\UnitTestCase;
use Drupal\pmi_fencing\EventSubscriber\FencingActivator;
use Symfony\Component\HttpFoundation\Request;
use Drupal\pmi_fencing\StringToBoolConverter;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Testing our FencingActivator service.
 */
class FencingActivatorTest extends UnitTestCase {

  private $fencingActivator;
  private $request;

  /**
   * The setup function.
   */
  protected function setUp(): void {
    $this->fencingActivator = new TestFencingActivator(new StringToBoolConverter());
    $this->request = new Request();
  }

  /**
   * When fencingHeaderEnabled=TRUE; noFencingGet=FALSE; => TRUE.
   */
  public function testHeaderTrueGetFalse() {
    // FencingHeaderEnabled=TRUE.
    $this->request->headers->set($this->fencingActivator::FENCING_ENABLED_HEADER_NAME, 'TRUE');
    // NoFencingGet=FALSE.
    $this->request->query->set($this->fencingActivator::NOFENCING_GET_VARIABLE_NAME, FALSE);

    $event = $this->createEventMockFromCurrentRequest();

    $changedEvent = $this->fencingActivator->checkSetCookie($event);
    $cookies = $changedEvent->getResponse()->headers->getCookies();

    $this->assertEquals(
      $cookies[0],
      new Cookie('fencing_overlay', TRUE)
    );
  }

  /**
   * When fencingHeaderEnabled=TRUE; noFencingGet=NOTSET; => TRUE.
   */
  public function testHeaderTrueGetNotset() {
    // FencingHeaderEnabled=TRUE.
    $this->request->headers->set($this->fencingActivator::FENCING_ENABLED_HEADER_NAME, 'TRUE');
    // NoFencingGet=NOTSET.
    $event = $this->createEventMockFromCurrentRequest();

    $changedEvent = $this->fencingActivator->checkSetCookie($event);
    $cookies = $changedEvent->getResponse()->headers->getCookies();

    $this->assertEquals(
      $cookies[0],
      new Cookie('fencing_overlay', TRUE)
    );
  }

  /**
   * When fencingHeaderEnabled=FALSE; noFencingGet=TRUE; => FALSE.
   */
  public function testHeaderFalseGetTrue() {
    // FencingHeaderEnabled=FALSE.
    $this->request->headers->set($this->fencingActivator::FENCING_ENABLED_HEADER_NAME, 'FALSE');
    // NoFencingGet=TRUE.
    $this->request->query->set($this->fencingActivator::NOFENCING_GET_VARIABLE_NAME, TRUE);

    $event = $this->createEventMockFromCurrentRequest();

    $changedEvent = $this->fencingActivator->checkSetCookie($event);
    $cookies = $changedEvent->getResponse()->headers->getCookies();

    $this->assertCount(
      0,
      $cookies
    );
  }

  /**
   * When fencingHeaderEnabled=FALSE; noFencingGet=NOTSET; => FALSE.
   */
  public function testHeaderFalseGetNotset() {
    // FencingHeaderEnabled=FALSE.
    $this->request->headers->set($this->fencingActivator::FENCING_ENABLED_HEADER_NAME, 'FALSE');
    // NoFencingGet=NOTSET.
    $event = $this->createEventMockFromCurrentRequest();

    $changedEvent = $this->fencingActivator->checkSetCookie($event);
    $cookies = $changedEvent->getResponse()->headers->getCookies();

    $this->assertCount(
      0,
      $cookies
    );
  }

  /**
   * When fencingHeaderEnabled=TRUE; noFencingGet=TRUE; => FALSE.
   */
  public function testHeaderTrueGetTrue() {
    // FencingHeaderEnabled=TRUE.
    $this->request->headers->set($this->fencingActivator::FENCING_ENABLED_HEADER_NAME, 'TRUE');
    // NoFencingGet=TRUE.
    $this->request->query->set($this->fencingActivator::NOFENCING_GET_VARIABLE_NAME, TRUE);

    $event = $this->createEventMockFromCurrentRequest();

    $changedEvent = $this->fencingActivator->checkSetCookie($event);
    $cookies = $changedEvent->getResponse()->headers->getCookies();

    $this->assertCount(
      0,
      $cookies
    );
  }

  /**
   * Creating an event mock from current request.
   */
  public function createEventMockFromCurrentRequest() {
    $event = $this->getMockBuilder(FilterResponseEvent::class)
      ->disableOriginalConstructor()
      ->setMethods(['setResponse', 'getRequest', 'getResponse'])
      ->getMock();
    $event->method('getRequest')->willReturn($this->request);
    $event->method('getResponse')->willReturn(new HtmlResponse());

    return $event;
  }

}

/**
 * A test double that actually returns the event object.
 */
class TestFencingActivator extends FencingActivator {

  /**
   * Making the function return the event object so that we could test it.
   */
  public function checkSetCookie($event) {
    parent::checkSetCookie($event);
    return $event;
  }

}
