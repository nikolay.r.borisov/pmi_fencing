<?php

namespace Drupal\Tests\pmi_fencing\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\pmi_fencing\StringToBoolConverter;

/**
 * Testing the string to bool converter.
 */
class StringToBoolConverterTest extends UnitTestCase {

  /**
   * An example of the bug without the service.
   *
   * @group stringtobool
   */
  public function testStringToBoolBug() {
    $this->assertNotEquals(
      FALSE,
      (bool) 'FALSE'
    );
  }

  /**
   * Test stringy FALSE to bool.
   *
   * @group stringtobool
   */
  public function testFalseStringToBool() {
    $stringToBoolConverter = new StringToBoolConverter();
    $this->assertFalse(
      $stringToBoolConverter->convert('FALSE')
    );
  }

  /**
   * Test stringy TRUE to bool.
   *
   * @group stringtobool
   */
  public function testTrueStringToBool() {
    $stringToBoolConverter = new StringToBoolConverter();
    $this->assertTrue(
        $stringToBoolConverter->convert('TRUE')
    );
  }

  /**
   * Test booly TRUE to bool.
   *
   * @group stringtobool
   */
  public function testTrueBoolToBool() {
    $stringToBoolConverter = new StringToBoolConverter();
    $this->assertTrue(
      $stringToBoolConverter->convert(TRUE)
    );
  }

  /**
   * Test booly FALSE to bool.
   *
   * @group stringtobool
   */
  public function testFalseBoolToBool() {
    $stringToBoolConverter = new StringToBoolConverter();
    $this->assertFalse(
      $stringToBoolConverter->convert(FALSE)
    );
  }

}
