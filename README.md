## What is this module doing?

This is a module that checks a header (set by the server) and then sets a cookie that the frontend could use to display an overlay.

If there is a specific GET variable passed to the request - the cookie is not set and the overlay is not triggered.
