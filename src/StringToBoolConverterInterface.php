<?php

namespace Drupal\pmi_fencing;

/**
 * Converting a String Boolean value to Boolean.
 */
interface StringToBoolConverterInterface {

  /**
   * The actual function that is converting to boolean.
   */
  public function convert($string);

}
