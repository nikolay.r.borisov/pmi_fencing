<?php

namespace Drupal\pmi_fencing\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\Cookie;
use Drupal\pmi_fencing\StringToBoolConverterInterface;

/**
 * A class that sets a cookie if fencing should be enabled.
 */
class FencingActivator implements EventSubscriberInterface {

  private $fencingEnabledHeader;
  private $noFencingGetVariable;
  private $stringToBoolConverter;

  const FENCING_ENABLED_HEADER_NAME = 'ecom-fencing-enabled';
  const NOFENCING_GET_VARIABLE_NAME = 'no_fencing';

  /**
   * Injecting some services here.
   */
  public function __construct(StringToBoolConverterInterface $stringToBoolConverter) {
    $this->stringToBoolConverter = $stringToBoolConverter;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => 'checkSetCookie',
    ];
  }

  /**
   * The callback function reacting on Kernel response event.
   */
  public function checkSetCookie($event) {

    $this->setFencingEnabledHeader($event->getRequest());
    $this->setNoFencingGetVariable($event->getRequest());

    if (
      $this->fencingEnabledHeaderIsTrue()
      && $this->noFencingGetVariableIsNotTrue()
    ) {
      $this->setFencingOverlayCookie($event);
    }

  }

  /**
   * Setting the Ecom Fencing header variable.
   */
  protected function setFencingEnabledHeader($request): void {
    $this->fencingEnabledHeader = $this->extractFencingEnabledHeader($request);
  }

  /**
   * Extracting from the header the value of Ecom Fencing.
   */
  private function extractFencingEnabledHeader($request): bool {
    if ($request->headers->has(self::FENCING_ENABLED_HEADER_NAME)) {
      return $this->stringToBoolConverter->convert(
        $request->headers->get(self::FENCING_ENABLED_HEADER_NAME)
      );
    }
    return FALSE;
  }

  /**
   * Setting the no_fencing get variable.
   */
  protected function setNoFencingGetVariable($request): void {
    $this->noFencingGetVariable = $this->extractNoFencingGetVariable($request);
  }

  /**
   * Extracting the no_fencing get variable.
   */
  private function extractNoFencingGetVariable($request): bool {
    if ($request->query->has(self::NOFENCING_GET_VARIABLE_NAME)) {
      return $this->stringToBoolConverter->convert(
        $request->query->get(self::NOFENCING_GET_VARIABLE_NAME)
      );
    }
    return FALSE;
  }

  /**
   * Setting the fencing overlay cookie.
   */
  protected function setFencingOverlayCookie($event) {
    $response = $event->getResponse();
    $response->headers->setCookie(
      new Cookie('fencing_overlay', TRUE)
    );
    $event->setResponse($response);
  }

  /**
   * Checking if the Enabled Header is true.
   */
  protected function fencingEnabledHeaderIsTrue(): bool {
    return $this->fencingEnabledHeader;
  }

  /**
   * Checking if the no_fencing get variable is not true.
   */
  protected function noFencingGetVariableIsNotTrue(): bool {
    return $this->noFencingGetVariable != TRUE;
  }

}
