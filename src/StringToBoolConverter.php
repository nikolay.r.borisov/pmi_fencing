<?php

namespace Drupal\pmi_fencing;

/**
 * Converting a String Boolean value to Boolean.
 */
class StringToBoolConverter implements StringToBoolConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function convert($string) {
    return filter_var($string, FILTER_VALIDATE_BOOLEAN);
  }

}
